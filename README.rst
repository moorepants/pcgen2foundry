

Tool to convert Pathfinder 1E PCGEN data stored in ``.lst`` files to ``.json``
database entries for FoundryVTT and it's Pathfinder 1E system.

Example: Ring of Protection +2
==============================

In PCGEN's source code you will find this ``.lst`` file::

   pcgen/data/pathfinder/paizo/roleplaying_game/core_rulebook/cr_equip_magic_items.lst

A single line in the file provides the information for the ring (among many
other magic items)::

   Ring of Protection +2					KEY:Ring of Protection +2					SORTKEY:Ring of Protection +2						TYPE:Magic.Ring	COST:8000												SOURCEPAGE:p.481																																																																BONUS:COMBAT|AC|2|TYPE=Deflection																																			QUALITY:Aura|faint abjuration							QUALITY:Caster Level|5th	QUALITY:Slot|ring		QUALITY:Construction Cost|4,000 gp		QUALITY:Construction Requirements|Forge Ring, shield of faith, caster must be of a level at least three times the bonus of the ring												DESC:This ring offers continual magical protection in the form of a deflection bonus of +1 to +5 to AC.

In a more easily readable form the line is::

   Ring of Protection +2				
      KEY:Ring of Protection +2				
      SORTKEY:Ring of Protection +2					
      TYPE:Magic.Ring
      COST:8000											
      SOURCEPAGE:p.481																																																															
      BONUS:COMBAT|AC|2|TYPE=Deflection																																		
      QUALITY:Aura|faint abjuration						
      QUALITY:Caster Level|5th
      QUALITY:Slot|ring	
      QUALITY:Construction Cost|4,000 gp	
      QUALITY:Construction Requirements|Forge Ring, shield of faith, caster must be of a level at least three times the bonus of the ring											
      DESC:This ring offers continual magical protection in the form of a deflection bonus of +1 to +5 to AC.

The FoundryVTT Pathfinder 1E system encodes the object in::

   foundryvtt-pathfinder1/packs/items/ring-of-protection-2.json

which has this as contents::

   {
   "_id": "0AIz2Gk74uaiLQIh",
   "name": "Ring of Protection +2",
   "permission": {
      "default": 0
   },
   "type": "equipment",
   "data": {
      "description": {
         "value": "<p style=\"box-sizing: border-box; user-select: text; color: #191813; font-size: 13px;\">This ring offers continual magical protection in the form of a&nbsp;<a style=\"box-sizing: border-box; user-select: text;\" href=\"https://www.d20pfsrd.com/basics-ability-scores/glossary#TOC-Bonus-Deflection-\">deflection</a>&nbsp;bonus of +1 to +5 to&nbsp;<a style=\"box-sizing: border-box; user-select: text;\" href=\"https://www.d20pfsrd.com/gamemastering/combat#TOC-Armor-Class\" rel=\"nofollow\">AC</a>.</p>",
         "chat": "",
         "unidentified": ""
      },
      "tags": [],
      "quantity": 1,
      "weight": 0,
      "price": 8000,
      "identified": true,
      "hp": {
         "max": 10,
         "value": 10
      },
      "hardness": 0,
      "carried": true,
      "unidentified": {
         "price": 0,
         "name": "Ring"
      },
      "identifiedName": "Ring of Protection +2",
      "cl": 6,
      "aura": {
         "custom": false,
         "school": "abj"
      },
      "activation": {
         "cost": null,
         "type": ""
      },
      "unchainedAction": {
         "activation": {
         "cost": 1,
         "type": ""
         }
      },
      "duration": {
         "value": null,
         "units": ""
      },
      "target": {
         "value": ""
      },
      "range": {
         "value": null,
         "units": "",
         "maxIncrements": 1,
         "minValue": null,
         "minUnits": ""
      },
      "uses": {
         "value": 0,
         "max": 0,
         "per": null,
         "autoDeductCharges": true,
         "autoDeductChargesCost": "1"
      },
      "changes": [
         {
         "_id": "kl3v8dsf",
         "formula": "2",
         "operator": "add",
         "subTarget": "ac",
         "modifier": "deflection",
         "priority": 0,
         "value": 0,
         "target": "ac"
         }
         ],
         "changeFlags": {
         "loseDexToAC": false,
         "noEncumbrance": false,
         "mediumArmorFullSpeed": false,
         "heavyArmorFullSpeed": false,
         "noStr": false,
         "noDex": false,
         "oneInt": false,
         "oneWis": false,
         "oneCha": false
         },
         "contextNotes": [],
         "measureTemplate": {
         "type": "",
         "size": "",
         "overrideColor": false,
         "customColor": "",
         "overrideTexture": false,
         "customTexture": ""
         },
         "attackName": "",
         "actionType": "",
         "attackBonus": "",
         "critConfirmBonus": "",
         "damage": {
         "parts": [],
         "critParts": [],
         "nonCritParts": []
         },
         "attackParts": [],
         "formulaicAttacks": {
         "count": {
         "formula": "",
         "value": null
         },
         "bonus": {
         "formula": ""
         },
         "label": null
         },
         "formula": "",
         "ability": {
         "attack": null,
         "damage": null,
         "damageMult": 1,
         "critRange": 20,
         "critMult": 2
         },
         "save": {
         "dc": 0,
         "type": "",
         "description": ""
         },
         "effectNotes": "",
         "attackNotes": "",
         "soundEffect": "",
         "links": {
         "children": [],
         "charges": []
         },
         "tag": "ringOfProtection2",
         "useCustomTag": false,
         "flags": {
         "boolean": [],
         "dictionary": []
         },
         "equipped": true,
         "equipmentType": "misc",
         "equipmentSubtype": "wondrous",
         "armor": {
         "value": 0,
         "dex": null,
         "acp": 0,
         "enh": 0
         },
         "spellFailure": 0,
         "slot": "ring",
         "masterwork": false,
         "size": "med",
         "broken": false,
         "nonlethal": false,
         "armorType": {
         "value": "",
         "_deprecated": true
         },
         "attack": {
         "parts": []
         }
         },
         "flags": {},
         "img": "systems/pf1/icons/items/jewelry/ring-iron.jpg",
         "effects": []
         }"""

This gives an idea of what this tool should do::

   >>> lst_entry = "Ring of Protection +2					KEY:Ring of Protection +2					SORTKEY:Ring of Protection +2						TYPE:Magic.Ring	COST:8000												SOURCEPAGE:p.481																																																																BONUS:COMBAT|AC|2|TYPE=Deflection																																			QUALITY:Aura|faint abjuration							QUALITY:Caster Level|5th	QUALITY:Slot|ring		QUALITY:Construction Cost|4,000 gp		QUALITY:Construction Requirements|Forge Ring, shield of faith, caster must be of a level at least three times the bonus of the ring												DESC:This ring offers continual magical protection in the form of a deflection bonus of +1 to +5 to AC."
   >>> lst_item = LSTItem.from_lst_str(lst_entry)
   >>> lst_item.cost
   '8000'
   >>> lst_item.desc
   'This ring offers continual magical protection in the form of a deflection bonus of +1 to +5 to AC.'
   >>> lst_item.key
   'Ring of Protection +2'
   >>> lst_item.name
   'Ring of Protection +2'
   >>> lst_item.sortkey
   'Ring of Protection +2'
   >>> lst_item.sourcepage
   'p.481'
   >>> lst_item.type
   'Magic.Ring'
   >>> lst_item.bonuses
   [{'category': 'COMBAT',
     'type': 'TYPE=Deflection',
     'value': '2',
     'attribute': 'AC'}]
   >>> lst_item.qualities
   [{'Aura': 'faint abjuration'},
    {'Caster Level': '5th'},
    {'Slot': 'ring'},
    {'Construction Cost': '4,000 gp'},
    {'Construction Requirements': 'Forge Ring, shield of faith, caster must be of a level at least three times the bonus of the ring'}]

This gives a simple idea of how the data can be converted to the "changes"
needed in Foundry::

   >>> print(lst_item.to_changes())
   "changes": [
   {
   "_id": "X",
   "formula": "2",
   "operator": "add",
   "subTarget": "ac",
   "modifier": "deflection",
   "priority": 0,
   "value": 0,
   "target": "ac"
   }
   ]
