import collections

# need a map from pcgen attributes to foundry pf1 attributes
ATTR_MAP = {
    'AC': 'ac',
}


class LSTItem:

    def __init__(self, **kwargs):

        for k, v in kwargs.items():
            setattr(self, k, v)

    @classmethod
    def from_lst_str(cls, lst_string):
        """
        lst_string : str
            Single line from lst file that represents an item.
        """

        lines = [s for s in lst_string.split('\t') if s != '']

        d = collections.defaultdict(list)

        for line in lines[1:]:
            # TODO : figure out what this PREVAREQ is
            if 'PREVAREQ' in line:
                line, _ = line.split('|PREVAREQ')
            key, val = line.split(':')
            if key == "QUALITY":
                qual_key, qual_val = val.split('|')
                qual_dict = {qual_key: qual_val}
                d['qualities'].append(qual_dict)
            elif key == "BONUS":
                # 0 : category
                # 1 : attribute bonus applies to
                # 2 : value of the bonus
                # 3 : type of bonus
                bonus_split = val.split('|')
                bonus_dict = {
                    'category': bonus_split[0],
                    'type': bonus_split[3],
                    'value': bonus_split[2],
                }
                bvars_list = bonus_split[1].split(',')
                for bvar in bvars_list:
                    cp_bonus_dict = bonus_dict.copy()
                    cp_bonus_dict['attribute'] = bvar
                    d['bonuses'].append(cp_bonus_dict)
            else:
                d[key.lower()] = val

        return cls(name=lines[0], **d)

    def to_changes(self):
        change_template = """\
{{
"_id": "X",
"formula": "{value}",
"operator": "add",
"subTarget": "{attribute}",
"modifier": "{type}",
"priority": 0,
"value": 0,
"target": "{attribute}"
}}"""

        changes = '"changes": [\n'
        for bonus in self.bonuses:
            changes += change_template.format(
                value=bonus['value'],
                attribute=ATTR_MAP[bonus['attribute']],
                type=bonus['type'].split('=')[1].lower()
            )
        changes += '\n]'

        return changes
