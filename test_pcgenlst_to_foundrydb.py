from pcgenlst_to_foundrydb import LSTItem


def test_armor():

    lst_entry = 'Padded Armor							KEY:Padded Armor (Base)			SORTKEY:zzBase				PROFICIENCY:ARMOR|Padded				TYPE:Armor.Light.ArmorProfLight.Suit.Nonmetal	COST:5	WT:10		ACCHECK:0																											MAXDEX:8		SOURCEPAGE:p.151	SPELLFAILURE:5																			BONUS:COMBAT|AC|1|TYPE=Armor|PREVAREQ:DisableArmorBonus,0																							BONUS:VAR|ArmorCheckPenalty|0|TYPE=BaseArmor																																																						DESC:Little more than heavy, quilted cloth, this armor provides only the most basic protection.																																																																																																																																							VISIBLE:NO'

    lst_item = LSTItem.from_lst_str(lst_entry)

    assert lst_item.cost == '5'
    assert lst_item.desc == 'Little more than heavy, quilted cloth, this armor provides only the most basic protection.'
    assert lst_item.key == 'Padded Armor (Base)'
    assert lst_item.sortkey == 'zzBase'
    assert lst_item.sourcepage == 'p.151'
    assert lst_item.type == 'Armor.Light.ArmorProfLight.Suit.Nonmetal'
    assert lst_item.wt == '10'
    assert lst_item.name == 'Padded Armor'
    assert lst_item.proficiency == 'ARMOR|Padded'
    assert lst_item.accheck == '0'
    assert lst_item.maxdex == '8'
    assert lst_item.spellfailure == '5'
    assert lst_item.visible == 'NO'
    expected_bonuses = [
        {
         'attribute': 'AC',
         'category': 'COMBAT',
         'type': 'TYPE=Armor',
         'value': '1',
         # PREVAREQ:DisableArmorBonus,0
        },
        {
         'attribute': 'ArmorCheckPenalty',
         'category': 'VAR',
         'type': 'TYPE=BaseArmor',
         'value': '0',
        },
    ]
    assert len(lst_item.bonuses) == len(expected_bonuses)
    for qal in lst_item.bonuses:
        assert qal in expected_bonuses


def test_amulet_of_might_fists():

    lst_entry = "Amulet of Mighty Fists +3				KEY:Amulet of Mighty Fists +3					SORTKEY:Amulet of Mighty Fists +3					TYPE:Magic.Wondrous.Neck.Amulet																												COST:36000	WT:0															SOURCEPAGE:p.496																																																																																																																																																																																																																																											BONUS:WEAPONPROF=TYPE.Natural|TOHIT,DAMAGE|3|TYPE=Enhancement																																																																				QUALITY:Aura|faint evocation									QUALITY:Caster Level|5th																QUALITY:Slot|neck					QUALITY:Construction Cost|18000		QUALITY:Construction Requirements|Craft Wondrous Item, greater magic fang, creator's caster level must be at least three times the amulet's bonus, plus any requirements of the melee weapon special abilities																																																																																																																																																					DESC:This amulet grants an enhancement bonus of +1 to +5 on attack and damage rolls with unarmed attacks and natural weapons.&nl;Alternatively, this amulet can grant melee weapon special abilities, so long as they can be applied to unarmed attacks. See Table 15-9 for a list of abilities. Special abilities count as additional bonuses for determining the market value of the item, but do not modify attack or damage bonuses. An amulet of mighty fists cannot have a modified bonus (enhancement bonus plus special ability bonus equivalents) higher than +5. An amulet of mighty fists does not need to have a +1 enhancement bonus to grant a melee weapon special ability."

    lst_item = LSTItem.from_lst_str(lst_entry)

    assert lst_item.cost == '36000'
    assert lst_item.desc == "This amulet grants an enhancement bonus of +1 to +5 on attack and damage rolls with unarmed attacks and natural weapons.&nl;Alternatively, this amulet can grant melee weapon special abilities, so long as they can be applied to unarmed attacks. See Table 15-9 for a list of abilities. Special abilities count as additional bonuses for determining the market value of the item, but do not modify attack or damage bonuses. An amulet of mighty fists cannot have a modified bonus (enhancement bonus plus special ability bonus equivalents) higher than +5. An amulet of mighty fists does not need to have a +1 enhancement bonus to grant a melee weapon special ability."
    assert lst_item.key == 'Amulet of Mighty Fists +3'
    assert lst_item.sortkey == 'Amulet of Mighty Fists +3'
    assert lst_item.sourcepage == 'p.496'
    assert lst_item.type == 'Magic.Wondrous.Neck.Amulet'
    assert lst_item.wt == '0'
    assert lst_item.name == 'Amulet of Mighty Fists +3'
    expected_bonuses = [
        {
         'attribute': 'TOHIT',
         'category': 'WEAPONPROF=TYPE.Natural',
         'type': 'TYPE=Enhancement',
         'value': '3',
        },
        {
         'attribute': 'DAMAGE',
         'category': 'WEAPONPROF=TYPE.Natural',
         'type': 'TYPE=Enhancement',
         'value': '3',
        }
    ]
    assert len(lst_item.bonuses) == len(expected_bonuses)
    for qal in lst_item.bonuses:
        assert qal in expected_bonuses
    expected_qualities = [
        {'Aura': 'faint evocation'},
        {'Caster Level': '5th'},
        {'Construction Cost': '18000'},
        {'Construction Requirements': "Craft Wondrous Item, greater magic fang, creator's caster level must be at least three times the amulet's bonus, plus any requirements of the melee weapon special abilities"},
        {'Slot': 'neck'},
    ]
    assert len(lst_item.qualities) == len(expected_qualities)
    for qal in lst_item.qualities:
        assert qal in expected_qualities


def test_ring_of_protection():
    lst_entry = "Ring of Protection +2					KEY:Ring of Protection +2					SORTKEY:Ring of Protection +2						TYPE:Magic.Ring	COST:8000												SOURCEPAGE:p.481																																																																BONUS:COMBAT|AC|2|TYPE=Deflection																																			QUALITY:Aura|faint abjuration							QUALITY:Caster Level|5th	QUALITY:Slot|ring		QUALITY:Construction Cost|4,000 gp		QUALITY:Construction Requirements|Forge Ring, shield of faith, caster must be of a level at least three times the bonus of the ring												DESC:This ring offers continual magical protection in the form of a deflection bonus of +1 to +5 to AC."
    readable_lst_entry = """\
Ring of Protection +2
	KEY:Ring of Protection +2
	SORTKEY:Ring of Protection +2
	TYPE:Magic.Ring
	COST:8000
	SOURCEPAGE:p.481
	BONUS:COMBAT|AC|2|TYPE=Deflection
	QUALITY:Aura|faint abjuration
	QUALITY:Caster Level|5th
	QUALITY:Slot|ring
	QUALITY:Construction Cost|4,000 gp
	QUALITY:Construction Requirements|Forge Ring, shield of faith, caster must be of a level at least three times the bonus of the ring
	DESC:This ring offers continual magical protection in the form of a deflection bonus of +1 to +5 to AC.\
"""

    lst_item = LSTItem.from_lst_str(lst_entry)
    assert lst_item.cost == '8000'
    assert lst_item.desc == 'This ring offers continual magical protection in the form of a deflection bonus of +1 to +5 to AC.'
    assert lst_item.key == 'Ring of Protection +2'
    assert lst_item.name == 'Ring of Protection +2'
    assert lst_item.sortkey == 'Ring of Protection +2'
    assert lst_item.sourcepage == 'p.481'
    assert lst_item.type == "Magic.Ring"
    expected_bonuses = [
        {
         'attribute': 'AC',
         'category': 'COMBAT',
         'type': 'TYPE=Deflection',
         'value': '2',
        },
    ]
    assert len(lst_item.bonuses) == len(expected_bonuses)
    for qal in lst_item.bonuses:
        assert qal in expected_bonuses
    expected_qualities = [
        {'Aura': 'faint abjuration'},
        {'Caster Level': '5th'},
        {'Slot': 'ring'},
        {'Construction Cost': '4,000 gp'},
        {'Construction Requirements': 'Forge Ring, shield of faith, caster must be of a level at least three times the bonus of the ring'},
    ]
    assert len(lst_item.qualities) == len(expected_qualities)
    for qal in lst_item.qualities:
        assert qal in expected_qualities

    expected_foundry_changes = """\
"changes": [
    {
    "_id": "kl3v8dsf",
    "formula": "2",
    "operator": "add",
    "subTarget": "ac",
    "modifier": "deflection",
    "priority": 0,
    "value": 0,
    "target": "ac"
    }
]"""

    assert lst_item.to_changes() == expected_foundry_changes

# ring of protection +2 from the foundry pf1 pack
    foundry_compendium_entry = """\
{
  "_id": "0AIz2Gk74uaiLQIh",
  "name": "Ring of Protection +2",
  "permission": {
    "default": 0
  },
  "type": "equipment",
  "data": {
    "description": {
      "value": "<p style=\"box-sizing: border-box; user-select: text; color: #191813; font-size: 13px;\">This ring offers continual magical protection in the form of a&nbsp;<a style=\"box-sizing: border-box; user-select: text;\" href=\"https://www.d20pfsrd.com/basics-ability-scores/glossary#TOC-Bonus-Deflection-\">deflection</a>&nbsp;bonus of +1 to +5 to&nbsp;<a style=\"box-sizing: border-box; user-select: text;\" href=\"https://www.d20pfsrd.com/gamemastering/combat#TOC-Armor-Class\" rel=\"nofollow\">AC</a>.</p>",
      "chat": "",
      "unidentified": ""
    },
    "tags": [],
    "quantity": 1,
    "weight": 0,
    "price": 8000,
    "identified": true,
    "hp": {
      "max": 10,
      "value": 10
    },
    "hardness": 0,
    "carried": true,
    "unidentified": {
      "price": 0,
      "name": "Ring"
    },
    "identifiedName": "Ring of Protection +2",
    "cl": 6,
    "aura": {
      "custom": false,
      "school": "abj"
    },
    "activation": {
      "cost": null,
      "type": ""
    },
    "unchainedAction": {
      "activation": {
        "cost": 1,
        "type": ""
      }
    },
    "duration": {
      "value": null,
      "units": ""
    },
    "target": {
      "value": ""
    },
    "range": {
      "value": null,
      "units": "",
      "maxIncrements": 1,
      "minValue": null,
      "minUnits": ""
    },
    "uses": {
      "value": 0,
      "max": 0,
      "per": null,
      "autoDeductCharges": true,
      "autoDeductChargesCost": "1"
    },
    "changes": [
      {
        "_id": "kl3v8dsf",
        "formula": "2",
        "operator": "add",
        "subTarget": "ac",
        "modifier": "deflection",
        "priority": 0,
        "value": 0,
        "target": "ac"
      }
    ],
    "changeFlags": {
      "loseDexToAC": false,
      "noEncumbrance": false,
      "mediumArmorFullSpeed": false,
      "heavyArmorFullSpeed": false,
      "noStr": false,
      "noDex": false,
      "oneInt": false,
      "oneWis": false,
      "oneCha": false
    },
    "contextNotes": [],
    "measureTemplate": {
      "type": "",
      "size": "",
      "overrideColor": false,
      "customColor": "",
      "overrideTexture": false,
      "customTexture": ""
    },
    "attackName": "",
    "actionType": "",
    "attackBonus": "",
    "critConfirmBonus": "",
    "damage": {
      "parts": [],
      "critParts": [],
      "nonCritParts": []
    },
    "attackParts": [],
    "formulaicAttacks": {
      "count": {
        "formula": "",
        "value": null
      },
      "bonus": {
        "formula": ""
      },
      "label": null
    },
    "formula": "",
    "ability": {
      "attack": null,
      "damage": null,
      "damageMult": 1,
      "critRange": 20,
      "critMult": 2
    },
    "save": {
      "dc": 0,
      "type": "",
      "description": ""
    },
    "effectNotes": "",
    "attackNotes": "",
    "soundEffect": "",
    "links": {
      "children": [],
      "charges": []
    },
    "tag": "ringOfProtection2",
    "useCustomTag": false,
    "flags": {
      "boolean": [],
      "dictionary": []
    },
    "equipped": true,
    "equipmentType": "misc",
    "equipmentSubtype": "wondrous",
    "armor": {
      "value": 0,
      "dex": null,
      "acp": 0,
      "enh": 0
    },
    "spellFailure": 0,
    "slot": "ring",
    "masterwork": false,
    "size": "med",
    "broken": false,
    "nonlethal": false,
    "armorType": {
      "value": "",
      "_deprecated": true
    },
    "attack": {
      "parts": []
    }
  },
  "flags": {},
  "img": "systems/pf1/icons/items/jewelry/ring-iron.jpg",
  "effects": []
}"""
